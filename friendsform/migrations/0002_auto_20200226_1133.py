# Generated by Django 3.0.3 on 2020-02-26 04:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendsform', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friends',
            name='Hobby',
            field=models.TextField(max_length=50),
        ),
        migrations.AlterField(
            model_name='friends',
            name='Name',
            field=models.TextField(max_length=50),
        ),
        migrations.AlterField(
            model_name='friends',
            name='Year',
            field=models.TextField(max_length=50),
        ),
    ]
