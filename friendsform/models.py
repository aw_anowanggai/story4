from django.db import models

# Create your models here.
class Friends(models.Model):
    Name = models.TextField(max_length=50)
    Year = models.TextField(max_length=50)
    Hobby = models.TextField(max_length=50)
