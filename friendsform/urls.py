from django.urls import path, include
from .views import Join

app_name = 'friendsform'

urlpatterns = [
    path('', Join, name='Friends')
]
