from django.shortcuts import render, redirect
from .models import Friends as friendsano
from .forms import FriendsFormPy

# Create your views here.



def Join(request):
    if request.method == "POST":
        form = FriendsFormPy(request.POST)
        if form.is_valid():
            fren = friendsano()
            fren.Name = form.cleaned_data['Name']
            fren.Year = form.cleaned_data['Year']
            fren.Hobby = form.cleaned_data['Hobby']
            fren.save()
        return redirect('/Friends')
    else:
       
        fren = friendsano.objects.all()
        form = FriendsFormPy()
        response = {"fren":fren, 'form':form}
        return render(request, 'Friends.html', response)

