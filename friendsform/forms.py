from django import forms

class FriendsFormPy(forms.Form):

    Name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Name',
        'type' :  'text',
        'required' : True,
    }))

    Year = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Year',
        'type' :  'text',
        'required' : True,
    }))

    Hobby = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Hobby',
        'type' :  'text',
        'required' : True,
    }))
