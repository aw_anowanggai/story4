from django import forms

class FriendsFormPy(forms.Form):

    Name = forms.NameField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Name',
        'type' :  'text',
        'required' : True,
    }))

    Year = forms.YearField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Year',
        'type' :  'text',
        'required' : True,
    }))

    Hobby = forms.HobbyField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Name',
        'type' :  'text',
        'required' : True,
    }))